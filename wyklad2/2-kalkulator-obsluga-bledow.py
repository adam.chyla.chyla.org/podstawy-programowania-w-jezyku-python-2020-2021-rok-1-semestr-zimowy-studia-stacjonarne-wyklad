xs = input('Podaj liczbe x:')

for znak in xs:
    if znak not in '0123456789':
        print("Blad, oczekiwano cyfry, a otrzymano:", znak)
        exit()

x = int(xs)

ys = input('Podaj liczbe y:')

for znak in ys:
    if znak not in '0123456789':
        print("Blad, oczekiwano cyfry, a otrzymano:", znak)
        exit()

y = int(ys)

op = input("Podaj operacje (+/-): ")

if op == '+':
    print("Wynik:", x + y)
elif op == '-':
    print("Wynik:", x - y)
else:
    print("Bledna opcja.")

