# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 1
# Ksiazka:
#   1. ala - 123456789
#   2. jan - 123458653
#
# Co gdy nikigo nie ma?
# Ksiazka jest pusta.
#
# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 2
# Podaj imie: Ala
# Podaj numer tel: 123456789
#
# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 3
# Koncze dzialanie.
#

def czy_ksiazka_zawiera_osoby(ksiazka_telefoniczna):
	return len(ksiazka_telefoniczna) > 0

def wypisz_cala_zawartosc(ksiazka_telefoniczna):
	if czy_ksiazka_zawiera_osoby(ksiazka_telefoniczna):
		print("Ksiazka:")
		for i, klucz in enumerate(ksiazka_telefoniczna):
			print("  " + str(i+1) + ". " + klucz + " - " + ksiazka_telefoniczna[klucz])
	else:
		print("Ksiazka jest pusta.")

def dodaj_nowa_osobe(ksiazka_telefoniczna):
	imie = input("Podaj imie: ")
	telefon = input("Numer telefonu: ")

	ksiazka_telefoniczna[imie] = telefon


def main():
	ksiazka = {}

	menu = '''1. Wypisanie calej ksiazki
2. Dodanie nowej osoby
3. Wyjscie z programu
'''

	czy_dzialac = True
	while czy_dzialac:
		print(menu)
		operacja = input("Wybor (1/2/3): ")

		if operacja == "1":
			wypisz_cala_zawartosc(ksiazka)
		elif operacja == "2":
			dodaj_nowa_osobe(ksiazka)
		elif operacja == "3":
			czy_dzialac = False
			print("Koncze dzialanie.")
		else:
			print("Bledna opcja.")
	

main()
