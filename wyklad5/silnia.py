# silnia - factorial
# 
# 5! = 1 * 2 * 3 * 4 * 5
#
# F(1) = 1
# F(n) = n * F(n-1)
# 
# F(5) = 5 * F(4) = 5 * (4 * F(3)) = 5 * 4 * (3 * F(3-1))
#      =  5 * 4 * 3 * (2 * F(1)) = 5 * 4 * 3 * 2 * 1
#

import matematyka

def main():
	w = matematyka.silnia(5)
	print("5! =", w)


if __name__ == "__main__":
	main()
