# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 1
# Ksiazka:
#   1. ala - 123456789
#   2. jan - 123458653
#
# Co gdy nikigo nie ma?
# Ksiazka jest pusta.
#
# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 2
# Podaj imie: Ala
# Podaj numer tel: 123456789
#
# 1. wypisanie calej ksiazki
# 2. dodanie nowej osoby
# 3. wyjscie z programu
# wybor (1/2/3): 3
# Koncze dzialanie.
#

# imie: nr tel
# Jan:123 456 789
# Ewa:789 123

import os.path


def deserialize(text):
    result = {}
    if text:
        for linia in text.split('\n'):
            imie_tel = linia.split(":")
            result[imie_tel[0]] = int(imie_tel[1])
    return result

assert deserialize("") == {}
assert deserialize("jan:456") == {'jan': 456}
assert deserialize("jan:345\nalicja:123") == {'jan': 345,
                                             'alicja': 123}

def serialize(phonebook):
    content = ""
    imiona = sorted(phonebook.keys())
    for imie in imiona:
        tel = phonebook[imie]
        content += f"{imie}:{tel}\n"
    return content.strip()

assert serialize({}) == ""
assert serialize({'jan': 123}) == "jan:123"
assert serialize({'jan':567,
                  'alicja':456}) == "alicja:456\njan:567"


#         /- slownik      /- napis
#        \/              \/
def save(phonebook, filename):
    with open(filename, 'w') as bookfile:
        content = serialize(phonebook)
        bookfile.write(content)

def load(filename):
    if os.path.exists(filename):
        with open(filename, "r") as file:
            file_content = file.read()
            return deserialize(file_content)
    else:
        return {}


ksiazka = load('book.txt')

menu = '''1. Wypisanie calej ksiazki
2. Dodanie nowej osoby
3. Wyjscie z programu
'''

czy_dzialac = True
while czy_dzialac:
    print(menu)
    operacja = input("Wybor (1/2/3): ")
    
    
    if operacja == "1":
        if len(ksiazka) > 0:
            print("Ksiazka:")
            for i, klucz in enumerate(ksiazka):
                print(f"  {i+1}. {klucz} - {ksiazka[klucz]}")
        else:
            print("Ksiazka jest pusta.")
    elif operacja == "2":
        imie = input("Podaj imie: ")
        telefon = input("Numer telefonu: ")
        
        ksiazka[imie] = telefon
    elif operacja == "3":
        czy_dzialac = False
        print("Koncze dzialanie.")
        save(ksiazka, 'book.txt')
    else:
        print("Bledna opcja.")
	


