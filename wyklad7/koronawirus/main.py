PAGE_URL = "https://koronawirusunas.pl/"

import urllib.request
from bs4 import BeautifulSoup


def download_webpage_source(url):
    with urllib.request.urlopen(url) as f:
        content = f.read()
    return content.decode('utf-8')


def parse(html):
    soup = BeautifulSoup(html, 'html.parser')
    badges = soup.find_all(class_="badge")

    liczba_zakazen = 0
    for b in badges:
        if b.get('title') == "Wszystkie potwierdzone zakażenia":
            text = b.get_text()
            liczba_zakazen = int(text.replace(' ', ''))

    return liczba_zakazen


def main():
    html = download_webpage_source(PAGE_URL)
    liczba_zakazonych = parse(html)
    print("liczba zakazonych:", liczba_zakazonych)


if __name__ == "__main__":
    main()

