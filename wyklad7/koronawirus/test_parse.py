from main import parse

def test_wybiera_badge_gdy_badge_jest_pierwszy():
    html_podstawowa_strona = """
<!doctype html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <title>Koronawirus w Polsce - aktualne dane, wykresy i mapa</title>
  </head>
  <body class="bs-example">
  <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
  </nav>

<main role="main">
  <div class="jumbotron">
    <div class="row align-items-center">
	        <div class="col-md-6 col-lg-8">
                  <h1 class="display-4 d-none d-lg-block">Koronawirus w Polsce (SARS-CoV-2)</h1>
                  <h1 class="display-5 d-lg-none">Koronawirus w Polsce<br>(SARS-CoV-2)</h1>
                  <p>Ostatnia aktualizacja 16.11.2020 o godzinie <strong>20:14</strong></p>
                  <hr width="40%" align="left">
                  
                  <span style="font-size: 2rem; font-weight: 500; line-height: 1.6;">Zakażenia: 
                      <br class="d-md-none">
                      <span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Wszystkie potwierdzone zakażenia"><i class="fas fa-notes-medical"></i> 733 788</span> 
                      <span class="badge badge-light">
                          <span class="badge badge-chorzy" data-toggle="tooltip" data-placement="top" title="Aktualnie zakażeni"><i class="fas fa-heart-broken"></i> 417 275</span> 
                          <span class="badge badge-dark" data-toggle="tooltip" data-placement="top" title="Zgony"><i class="fas fa-cross"></i> 10 491</span> 
                          <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Wyleczeni"><i class="fas fa-heart"></i> 306 022</span> 
                      </span>
                  </span>
		    </div>
    </div>
  </div>
</main>

</body>
</html>
"""

    liczba_zakazen = parse(html_podstawowa_strona)
    assert liczba_zakazen == 733788, f"Bledna liczba zakazen w tescie, wynik: {liczba_zakazen}"


def test_wybiera_bagde_gdy_badge_nie_jest_pierwszy():
    html_bagde_nie_jest_pierwszy = """
<!doctype html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <title>Koronawirus w Polsce - aktualne dane, wykresy i mapa</title>
  </head>
  <body class="bs-example">
  <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
  </nav>

<main role="main">
  <div class="jumbotron">
    <div class="row align-items-center">
	        <div class="col-md-6 col-lg-8">
                  <h1 class="display-4 d-none d-lg-block">Koronawirus w Polsce (SARS-CoV-2)</h1>
                  <h1 class="display-5 d-lg-none">Koronawirus w Polsce<br>(SARS-CoV-2)</h1>
                  <p>Ostatnia aktualizacja 16.11.2020 o godzinie <strong>20:14</strong></p>
                  <hr width="40%" align="left">
                  
                  <span style="font-size: 2rem; font-weight: 500; line-height: 1.6;">Zakażenia: 
                      <br class="d-md-none">
                      <span class="badge badge-light">
                          <span class="badge badge-chorzy" data-toggle="tooltip" data-placement="top" title="Aktualnie zakażeni"><i class="fas fa-heart-broken"></i> 417 275</span> 
                          <span class="badge badge-dark" data-toggle="tooltip" data-placement="top" title="Zgony"><i class="fas fa-cross"></i> 10 491</span> 
                          <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Wyleczeni"><i class="fas fa-heart"></i> 306 022</span> 
                      </span>
                      <span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Wszystkie potwierdzone zakażenia"><i class="fas fa-notes-medical"></i> 733 788</span> 
                  </span>
		    </div>
    </div>
  </div>
</main>

</body>
</html>
"""

    liczba_zakazen = parse(html_bagde_nie_jest_pierwszy)
    assert liczba_zakazen == 733788, f"Bledna liczba zakazen w tescie, wynik: {liczba_zakazen}"


def test_brak_liczby_zakazen():
    html_bagde_nie_jest_pierwszy = """
    <!doctype html>
    <html lang="pl">
      <head>
        <meta charset="utf-8">
        <title>Koronawirus w Polsce - aktualne dane, wykresy i mapa</title>
      </head>
      <body class="bs-example">
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
      </nav>

    <main role="main">
      <div class="jumbotron">
        <div class="row align-items-center">
    	        <div class="col-md-6 col-lg-8">
                      <h1 class="display-4 d-none d-lg-block">Koronawirus w Polsce (SARS-CoV-2)</h1>
                      <h1 class="display-5 d-lg-none">Koronawirus w Polsce<br>(SARS-CoV-2)</h1>
                      <p>Ostatnia aktualizacja 16.11.2020 o godzinie <strong>20:14</strong></p>
                      <hr width="40%" align="left">

                      <span style="font-size: 2rem; font-weight: 500; line-height: 1.6;">Zakażenia: 
                          <br class="d-md-none">
                          <span class="badge badge-light">
                              <span class="badge badge-chorzy" data-toggle="tooltip" data-placement="top" title="Aktualnie zakażeni"><i class="fas fa-heart-broken"></i> 417 275</span> 
                              <span class="badge badge-dark" data-toggle="tooltip" data-placement="top" title="Zgony"><i class="fas fa-cross"></i> 10 491</span> 
                              <span class="badge badge-success" data-toggle="tooltip" data-placement="top" title="Wyleczeni"><i class="fas fa-heart"></i> 306 022</span> 
                          </span>
                      </span>
    		    </div>
        </div>
      </div>
    </main>

    </body>
    </html>
    """

    liczba_zakazen = parse(html_bagde_nie_jest_pierwszy)
    assert liczba_zakazen == 0, f"Bledna liczba zakazen w tescie, wynik: {liczba_zakazen}"


test_wybiera_badge_gdy_badge_jest_pierwszy()
test_wybiera_bagde_gdy_badge_nie_jest_pierwszy()
test_brak_liczby_zakazen()

